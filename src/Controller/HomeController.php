<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Entity\Ville;
use App\Form\ModificationMotDePasseType;
use App\Form\ModificationProfilByUserType;
use App\Repository\EtatRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;



class HomeController extends AbstractController
{

    /**
     * @Route("/", name="connect")
     */
    public function connect(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('authentificateur/connect.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/home", name="home")
     */
    public function home(Request $request,Session $session): Response
    {

        return $this->render('home/homepage.html.twig');
    }






}

<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Form\CreateLieuType;
use App\Repository\VilleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/home/lieu")
 */
class LieuController extends AbstractController
{
    /**
     * @Route("/new", name="newLieu")
     */
    public function ajoutlieu(Request $request,
                              Session $session,
                              VilleRepository $repository): Response
    {
        $lieu = new Lieu();
        $form = $this->createForm(CreateLieuType::class,$lieu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $lieu->setVille($repository->findOneBy(['id' => $form->get("ville")->getData()]));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lieu);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }
        return $this->render('lieu/lieu.html.twig',[
            'lieuform' => $form->createView()
        ]);
    }
}

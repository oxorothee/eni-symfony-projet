<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Form\CampuseType;
use App\Form\CreateCampusType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function dashboard(): Response
    {
        return $this->render('admin/dashboard.html.twig', [
        ]);
    }

    /**
     * @Route("/new/campus", name="newCampus")
     */
    public function addCampus(Request $request): Response
    {
        $campus = new Campus();
        $form = $this->createForm(CreateCampusType::class,$campus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($campus);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }

        return $this->render('admin/newCampus.html.twig', [
            "campusForm" => $form->createView()
        ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Ville;
use App\Form\CreateVilleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/Ville")
 */
class VilleController extends AbstractController
{

    /**
     * @Route("/", name="Ville")
     */
    public function Ville(Request $request,Session $session): Response
    {
        return $this->render('ville/ville.html.twig');
    }


    /**
     * @Route("/new", name="newVille")
     */
    public function ajoutVille(Request $request,Session $session): Response
    {
        $ville = new Ville();
        $form = $this->createForm(CreateVilleType::class,$ville);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ville);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }
        return $this->render('ville/newville.html.twig',[
            'villeform' => $form->createView()
        ]);
    }
}

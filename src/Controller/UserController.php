<?php

namespace App\Controller;

use App\Form\ModificationMotDePasseType;
use App\Form\ModificationProfilByUserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/home/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="profil")
     */
    public function profil(Request $request,
                           Session $session,
                           UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = $this->getUser();

        /*
         *  formulaire de modification des infos utilisateur
         */
        $modificationProfil = $this->createForm(ModificationProfilByUserType::class,$user);
        $modificationProfil->handleRequest($request);

        if ($modificationProfil->isSubmitted() && $modificationProfil->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            # todoo message flash succes
        }

        /*
         *  formulaire de modification du mot de passe
         */
        $modificationMDP= $this->createForm(ModificationMotDePasseType::class);
        $modificationMDP->handleRequest($request);

        if ($modificationMDP->isSubmitted() && $modificationMDP->isValid()){

            $mdp = $modificationMDP->get("motDePasse")->getData();
            $confirme = $modificationMDP->get("confirmation")->getData();
            $encode = $passwordEncoder->encodePassword($user, $mdp);

            if ( $mdp == $confirme){
                $user->setPassword($encode);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
                # todoo message flash succes
            }
            else{
                #todoo message flash echec
            }
        }

        return $this->render('user/profil.html.twig',[
            'mofificationForm' => $modificationProfil->createView(),
            'passawordForm' => $modificationMDP->createView()
        ]);
    }
}

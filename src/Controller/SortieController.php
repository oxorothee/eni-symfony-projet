<?php

namespace App\Controller;

use App\Entity\Sortie;
use App\Entity\User;
use App\Form\CreateSortieType;
use App\Repository\EtatRepository;
use App\Repository\LieuRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/home/sortie")
 */
class SortieController extends AbstractController
{

    /**
     * @Route("/", name="Sortie")
     */
    public function sortie()
    {
        # todoo créé une page avec les sortie existante, montrer les sortie que la personne organise ou celle auqu'elle il participe
        return $this->render('home/sorti.html.twig');
    }

    /**
     * @Route("/new", name="newSortie")
     */
    public function ajoutsortie(Request $request,
                               Session $session,
                               EtatRepository $etatRepository,
                               UserRepository $userRepository,
                               LieuRepository $lieuRepository): Response
    {
        $sortie = new Sortie();
        $form = $this->createForm(CreateSortieType::class,$sortie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $user = $this->getUser();

            $sortie->setOrganisateur($user);
            $sortie->setEtat($etatRepository->findOneBy(["libelle" => 'créé']));
            $sortie->setLieu($lieuRepository->findOneBy(['id' => $form->get('lieu')->getData()]));


            $sortie->setCampus($user->getCampus());


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sortie);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }
        return $this->render('sortie/sorti.html.twig',[
            'sortiform' => $form->createView()
        ]);
    }

}

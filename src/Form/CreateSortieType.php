<?php

namespace App\Form;

use App\Entity\Lieu;
use App\Entity\Sortie;
use App\Repository\CampusRepository;
use App\Repository\LieuRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateSortieType extends AbstractType
{

    private $lieurepository;
    private $campusrepository;

    public function __construct(LieuRepository $Repository, CampusRepository $campusRepository){
        $this->lieurepository = $Repository;
        $this->campusrepository = $campusRepository;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('infosSortie')
            ->add('dateHeureDebut')
            ->add('dateHeureFin')
            ->add('dateLimiteInscription')
            ->add('nbInscriptionMax')
            ->add('lieu', EntityType::class, [
                'class' => Lieu::class,
                'choices' => $this->lieurepository->findAll(),
                'choice_label' => 'nom',
                'mapped' => false,
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
        ]);
    }
}
